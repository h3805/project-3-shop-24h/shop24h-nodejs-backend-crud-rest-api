//import express
const express = require("express");
//import mongoose
var mongoose = require('mongoose');
//import path
const path = require("path");
//import router
const ProductTypeRouter = require("./app/routes/ProductTypeRouter")
const CustomerRouter = require("./app/routes/CustomerRouter")
const OrderRouter = require("./app/routes/OrderRouter")
const ProductRouter = require("./app/routes/ProductRouter")
//khởi tạo app express
const app = express();
app.use(express.json());
//khai báo cổng
const port = 8000;
//Kết nối với MongoDB
mongoose.connect("mongodb://localhost:27017/CRUD_Shop24h", (error) => {
    if (error) throw error;
    console.log('Successfully connected');
})

//<- Middleware
app.use((request, response, next) => {
    console.log("Request method: ", request.method);
    next();
})
//cấu hình Cross Origin Middleware
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

//->End Middleware

//<-controllers
app.get("/", (request, response) => {
    response.json({
        message: "Hello world"
    })
});
//->end controllers

//Router level middleware
app.use("/", ProductTypeRouter);
app.use("/", CustomerRouter);
app.use("/", OrderRouter);
app.use("/", ProductRouter);
//listen port
app.listen(port, () => {
    console.log("App listen on Port: ", port);
})