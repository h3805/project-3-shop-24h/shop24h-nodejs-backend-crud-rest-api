//import mongoose
const mongoose = require('mongoose');
//khai báo Schema lấy từ thư viện mongoose
const Schema = mongoose.Schema;
//khai báo một Schema với các thuộc tính yêu cầu
const ProductSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId,
    },
    name: {
        type: String,
        unique: true,
        required: true,
    },
    description: {
        type: String,
    },
    type: {
        type: mongoose.Types.ObjectId,
        ref: "ProductType",
        required: true
    },
    imageUrl: {
        type: String,
        required: true,
    },
    buyPrice: {
        type: Number,
        required: true,
    },
    promotionPrice: {
        type: Number,
        required: true,
    },
    amount: {
        type: Number,
        default: 0
    },
    
    timeCreated: {
        type: Date,
        default: Date.now(),
    },
    timeUpdated: {
        type: Date,
        default: Date.now(),
    },
})
// exports ra một model nhờ Schema vừa khai báo
module.exports = mongoose.model("Product", ProductSchema);