//import mongoose
const mongoose = require('mongoose');
//khai báo Schema lấy từ thư viện mongoose
const Schema = mongoose.Schema;
//khai báo một Schema với các thuộc tính yêu cầu
const OrderSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId,
    },
    orderDate: {
        type: Date,
        default: Date.now(),
    },
    shippedDate: {
        type: Date,
    },
    note: {
        type: String,
    },
    orderDetail: [],
    cost: {
        type: Number,
        default: 0,
    },
    timeCreated: {
        type: Date,
        default: Date.now(),
    },
    timeUpdated: {
        type: Date,
        default: Date.now(),
    },
})

// exports ra một model nhờ Schema vừa khai báo
module.exports = mongoose.model("Order", OrderSchema);