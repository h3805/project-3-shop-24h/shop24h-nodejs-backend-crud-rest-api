//import mongoose
const mongoose = require('mongoose');
//khai báo Schema lấy từ thư viện mongoose
const Schema = mongoose.Schema;
//khai báo một Schema với các thuộc tính yêu cầu
const CustomerSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId,
    },
    fullName: {
        type: String,
        required: true,
    },
    phone: {
        type: String,
        required: true,
        unique: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    address: {
        type: String,
        default: "",
    },
    city: {
        type: String,
        default: "",
    },
    country: {
        type: String,
        default: "",
    },
    orders: [{
        type: mongoose.Types.ObjectId,
        ref: "Order",
    }],
    timeCreated: {
        type: Date,
        default: Date.now(),
    },
    timeUpdated: {
        type: Date,
        default: Date.now(),
    },
})

// exports ra một model nhờ Schema vừa khai báo
module.exports = mongoose.model("Customer", CustomerSchema);