//import mongoose
const mongoose = require('mongoose');
//khai báo Schema lấy từ thư viện mongoose
const Schema = mongoose.Schema;
//khai báo một Schema với các thuộc tính yêu cầu
const ProductTypeSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId,
    },
    name: {
        type: String,
        unique: true,
        required: true,
    },
    description: {
        type: String,
    },
    timeCreated: {
        type: Date,
        default: Date.now(),
    },
    timeUpdated: {
        type: Date,
        default: Date.now(),
    },
})
// exports ra một model nhờ Schema vừa khai báo
module.exports = mongoose.model("ProductType", ProductTypeSchema);