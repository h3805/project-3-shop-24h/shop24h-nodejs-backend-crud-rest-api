//Khai báo mongoose
const mongoose = require("mongoose");
//import module models 
const ProductTypeModel = require("../model/ProductTypeModel");

//Hàm tạo product type
const createProductType = (request, response) => {
    //khai báo biến lấy dữ liệu từ request body json
    let name = request.body.name;
    let description = request.body.description;

    //validate
    if (!name) {
        return response.status(500).json({ //status 500: Khi code bị lỗi
            "status": "Bad request",
            "messager": "name is reqrired",
        })
    }

    //.create Thêm dữ liệu vào collection
    ProductTypeModel.create({
        _id: mongoose.Types.ObjectId(),
        name: name,
        description: description,
    }, (error, data) => {
        if (error) {
            return response.status(500).json({ //status 500: Khi code bị lỗi
                "status": "Internal server error",
                "messager": error.message,
            })
        }
        else {
            return response.status(201).json({ //status 201: Khi tạo dữ liệu thành công
                "status": "Created!",
                "data": data,
            })
        }
    })
}

//Hàm lấy toàn bộ danh sách product type
const getAllProductType = (request, response) => {
    //lấy dữ liệu từ request param
    let { typename } = request.query;
    //tạo mảng chứa danh sách điều kiện lọc
    const condition = {};

    if (typename) {
        const regex = new RegExp(`${typename}`, `i`) //tạo regex /{typename}/i
        condition.name = regex // /{typename}/i : Tìm các collection có thuộc tính name chứa chữ “{typename}”
    }
    //.find Lấy dữ liệu từ collection
    ProductTypeModel.find(condition).exec((error, data) => {
        if (error) {
            return response.status(500).json({ //status 500: Khi code bị lỗi
                "status": "Internal server error",
                "messager": error.message,
            })
        }
        else {
            return response.status(200).json({ //status 200: Khi response trả về thành công
                "status": "Success",
                "data": data,
            })
        }
    })
}

//Hàm lấy thông tin product type bởi productTypeId
const getProductTypeById = (request, response) => {
    //lấy id từ request param
    let ProductTypeId = request.params.ProductTypeId;
    // Kiểm tra ProductTypeId có phải ObjectId hay không
    if (!mongoose.Types.ObjectId.isValid(ProductTypeId)) {
        return response.status(400).json({ //status 400: Khi input truyền vào từ request (query, params, body JSON) không hợp lệ
            status: "Bad request",
            message: "ProductTypeId is not valid"
        })
    }

    //.findById Lấy dữ liệu từ collection theo id
    ProductTypeModel.findById(ProductTypeId, (error, data) => {
        if (error) {
            return response.status(500).json({ //status 500: Khi code bị lỗi
                status: "Internal server error",
                message: error.message
            })
        } else {
            if (data) {
                return response.status(200).json({ //status 200: Khi response trả về thành công
                    status: "Success",
                    data: data
                })
            } else {
                return response.status(404).json({ //status 404: Khi không tìm thấy bản ghi trên CSDL
                    status: "Not found"
                })
            }
        }
    })
}

//hàm cập nhật thông tin product type
const updateProductType = (request, response) => {
    //lấy id từ request param
    let ProductTypeId = request.params.ProductTypeId;
    // Kiểm tra ProductTypeId có phải ObjectId hay không
    if (!mongoose.Types.ObjectId.isValid(ProductTypeId)) {
        return response.status(400).json({
            status: "Bad request",
            message: "ProductTypeId is not valid"
        })
    }

    //khai báo biến lấy dữ liệu từ request body json
    let { name, description } = request.body;

    //.findByIdAndUpdate Cập nhật dữ liệu trên collection theo id
    ProductTypeModel.findByIdAndUpdate(ProductTypeId, {
        name: name,
        description: description,
        timeUpdated: Date.now(),
    }, (error, data) => {
        if (error) {
            return response.status(500).json({ //status 500: Khi code bị lỗi
                status: "Internal server error",
                message: error.message
            })
        } else {
            if (data) {
                return response.status(200).json({ //status 200: Khi response trả về thành công
                    status: "Success",
                    data: data
                })
            } else {
                return response.status(404).json({ //status 404: Khi không tìm thấy bản ghi trên CSDL
                    status: "Not found"
                })
            }
        }
    })
}

//hàm xóa thông tin product type
const deleteProductType = (request, response) => {
    //lấy id từ request param
    let ProductTypeId = request.params.ProductTypeId;
    // Kiểm tra ProductTypeId có phải ObjectId hay không
    if (!mongoose.Types.ObjectId.isValid(ProductTypeId)) {
        return response.status(400).json({ //status 400: Khi input truyền vào từ request (query, params, body JSON) không hợp lệ
            status: "Bad request",
            message: "ProductTypeId is not valid"
        })
    }

    //.findByIdAndDelete Xóa dữ liệu trên collection theo id
    ProductTypeModel.findByIdAndDelete(ProductTypeId, (error) => {
        if (error) {
            return response.status(500).json({ //status 500: Khi code bị lỗi
                status: "Internal server error",
                message: error.message
            })
        } else {
            return response.status(204).json() //status 204: Khi xóa thành công
        }
    })
}

//export module
module.exports = { createProductType, getAllProductType, getProductTypeById, updateProductType, deleteProductType };