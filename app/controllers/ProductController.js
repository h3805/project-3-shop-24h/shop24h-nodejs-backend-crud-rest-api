//Khai báo mongoose
const mongoose = require("mongoose");
//khai báo MongoClient để kết nối với máy chủ mongodb
const MongoClient = require('mongodb').MongoClient;
//import module models 
const ProductModel = require("../model/ProductModel");

//Khai báo mảng dữ liệu chứa thông tin collection producttypes
let productTypeData = [];
//kết nối với máy chủ mongodb
MongoClient.connect("mongodb://localhost:27017/", (error, db) => {
    if (error) throw error;
    var dbo = db.db("CRUD_Shop24h");
    dbo.collection("producttypes").find({}).toArray((error, result) => { //Lấy tất cả dữ liệu đang có trong collection producttypes
        if (error) throw error;
        productTypeData = result;
        db.close(); //Đóng kết nối
    });
});

//Hàm tạo product
const createProduct = (request, response) => {
    //khai báo biến lấy dữ liệu từ request body json
    let name = request.body.name;
    let description = request.body.description;
    let type = request.body.type;
    let imageUrl = request.body.imageUrl;
    let buyPrice = request.body.buyPrice;
    let promotionPrice = request.body.promotionPrice;
    let amount = request.body.amount;
    let typeId = null;

    //validate
    if (!name) {
        return response.status(500).json({ //status 500: Khi code bị lỗi
            "status": "Bad request",
            "messager": "name is reqrired",
        })
    }
    if (!type) {
        return response.status(500).json({ //status 500: Khi code bị lỗi
            "status": "Bad request",
            "messager": "type is reqrired",
        })
    }
    if (!imageUrl) {
        return response.status(500).json({ //status 500: Khi code bị lỗi
            "status": "Bad request",
            "messager": "imageUrl is reqrired",
        })
    }
    if (!buyPrice) {
        return response.status(500).json({ //status 500: Khi code bị lỗi
            "status": "Bad request",
            "messager": "buyPrice is reqrired",
        })
    }
    if (!promotionPrice) {
        return response.status(500).json({ //status 500: Khi code bị lỗi
            "status": "Bad request",
            "messager": "promotionPrice is reqrired",
        })
    }

    //tìm type trong mảng dữ liệu productType, nếu có gán giá trị cho typeId
    for (let bI = 0; bI < productTypeData.length; bI++) {
        if (type.toUpperCase() == productTypeData[bI].name.toUpperCase()) {
            typeId = productTypeData[bI]._id;
        }
    }
    if (typeId == null) {
        return response.status(404).json({ //status 404: Khi không tìm thấy bản ghi trên CSDL
            status: "Not found type"
        })
    }

    //.create Thêm dữ liệu vào collection
    ProductModel.create({
        _id: mongoose.Types.ObjectId(),
        name: name,
        description: description,
        type: typeId,
        imageUrl: imageUrl,
        buyPrice: buyPrice,
        promotionPrice: promotionPrice,
        amount: amount,
    }, (error, data) => {
        if (error) {
            return response.status(500).json({ //status 500: Khi code bị lỗi
                "status": "Internal server error",
                "messager": error.message,
            })
        }
        else {
            return response.status(201).json({ //status 201: Khi tạo dữ liệu thành công
                "status": "Created!",
                "data": data,
            })
        }
    })
}

//Hàm lấy toàn bộ danh sách product
const getAllProduct = (request, response) => {
    //lấy dữ liệu từ request param
    let { limit, name, type, minPromotionPrice, maxPromotionPrice, skip } = request.query;
    //tạo mảng chứa danh sách điều kiện lọc
    const condition = {};


    if (name) {
        const regex = new RegExp(`${name}`, `i`) //tạo regex /{name}/i
        condition.name = regex // /{name}/i : Tìm các collection có thuộc tính name chứa chữ “{name}”
    }

    if (type !== undefined && !Array.isArray(type)) {
        type = [type]
    }

    if (Array.isArray(type)) {
        condition.type = {
            $in: type // { type: { $in: [value1, value2,... ] } } // Tìm các collection có thuộc tính type có giá trị nằm trong mảng
        }
    }

    if (minPromotionPrice) {
        condition.promotionPrice = {
            ...condition.promotionPrice,
            $gte: minPromotionPrice //$gte: lớn hơn hoặc bằng
        }
    }

    if (maxPromotionPrice) {
        condition.promotionPrice = {
            ...condition.promotionPrice,
            $lte: maxPromotionPrice //$lte: nhỏ hơn hoặc bằng
        }
    }

    ProductModel.find(condition).sort({ timeCreated: -1 }).skip(parseInt(skip) || 0).limit(parseInt(limit) || 0).exec((error, data) => {
        if (error) {
            return response.status(500).json({ //status 500: Khi code bị lỗi
                "status": "Internal server error",
                "messager": error.message,
            })
        }
        else {
            return response.status(200).json({ //status 200: Khi response trả về thành công
                "status": "Success",
                "data": data
            })
        }
    })

}

//Hàm lấy thông tin product bởi productId
const getProductById = (request, response) => {
    //lấy id từ request param
    let ProductId = request.params.ProductId;
    // Kiểm tra ProductId có phải ObjectId hay không
    if (!mongoose.Types.ObjectId.isValid(ProductId)) {
        return response.status(400).json({ //status 400: Khi input truyền vào từ request (query, params, body JSON) không hợp lệ
            status: "Bad request",
            message: "ProductId is not valid"
        })
    }

    //.findById Lấy dữ liệu từ collection theo id
    ProductModel.findById(ProductId, (error, data) => {
        if (error) {
            return response.status(500).json({ //status 500: Khi code bị lỗi
                status: "Internal server error",
                message: error.message
            })
        } else {
            if (data) {
                return response.status(200).json({ //status 200: Khi response trả về thành công
                    status: "Success",
                    data: data
                })
            } else {
                return response.status(404).json({ //status 404: Khi không tìm thấy bản ghi trên CSDL
                    status: "Not found"
                })
            }
        }
    })
}

//hàm cập nhật thông tin product
const updateProduct = (request, response) => {
    //lấy id từ request param
    let ProductId = request.params.ProductId;
    // Kiểm tra ProductId có phải ObjectId hay không
    if (!mongoose.Types.ObjectId.isValid(ProductId)) {
        return response.status(400).json({ //status 400: Khi input truyền vào từ request (query, params, body JSON) không hợp lệ
            status: "Bad request",
            message: "ProductId is not valid"
        })
    }

    //khai báo biến lấy dữ liệu từ request body json
    let { name, description, type, imageUrl, buyPrice, promotionPrice, amount } = request.body;
    let typeId = null;

    //nếu có nhập type trong request body, kiểm tra type name có nằm trong collection producttypes hay ko trả kết quả về typeId
    if (type !== undefined && type !== "") {
        for (let bI = 0; bI < productTypeData.length; bI++) {
            if (type.toUpperCase() == productTypeData[bI].name.toUpperCase()) {
                typeId = productTypeData[bI]._id;
            }
        }
        if (typeId == null) {
            return response.status(404).json({ //status 404: Khi không tìm thấy bản ghi trên CSDL
                status: "Not found type"
            })
        }
    }

    if (typeId !== null) {
        //.findByIdAndUpdate Cập nhật dữ liệu trên collection theo id
        ProductModel.findByIdAndUpdate(ProductId, {
            name: name,
            description: description,
            type: typeId,
            imageUrl: imageUrl,
            buyPrice: buyPrice,
            promotionPrice: promotionPrice,
            amount: amount,
            timeUpdated: Date.now(),
        }, (error, data) => {
            if (error) {
                return response.status(500).json({ //status 500: Khi code bị lỗi
                    status: "Internal server error",
                    message: error.message
                })
            } else {
                if (data) {
                    return response.status(200).json({ //status 200: Khi response trả về thành công
                        status: "Success",
                        data: data
                    })
                } else {
                    return response.status(404).json({ //status 404: Khi không tìm thấy bản ghi trên CSDL
                        status: "Not found"
                    })
                }
            }
        })
    }
    else {
        //.findByIdAndUpdate Cập nhật dữ liệu trên collection theo id
        ProductModel.findByIdAndUpdate(ProductId, {
            name: name,
            description: description,
            imageUrl: imageUrl,
            buyPrice: buyPrice,
            promotionPrice: promotionPrice,
            amount: amount,
            timeUpdated: Date.now(),
        }, (error, data) => {
            if (error) {
                return response.status(500).json({ //status 500: Khi code bị lỗi
                    status: "Internal server error",
                    message: error.message
                })
            } else {
                if (data) {
                    return response.status(200).json({ //status 200: Khi response trả về thành công
                        status: "Success",
                        data: data
                    })
                } else {
                    return response.status(404).json({ //status 404: Khi không tìm thấy bản ghi trên CSDL
                        status: "Not found"
                    })
                }
            }
        })
    }


}

//hàm xóa thông tin product
const deleteProduct = (request, response) => {
    //lấy id từ request param
    let ProductId = request.params.ProductId;
    // Kiểm tra ProductId có phải ObjectId hay không
    if (!mongoose.Types.ObjectId.isValid(ProductId)) {
        return response.status(400).json({ //status 400: Khi input truyền vào từ request (query, params, body JSON) không hợp lệ
            status: "Bad request",
            message: "ProductId is not valid"
        })
    }

    //.findByIdAndDelete Xóa dữ liệu trên collection theo id
    ProductModel.findByIdAndDelete(ProductId, (error) => {
        if (error) {
            return response.status(500).json({ //status 500: Khi code bị lỗi
                status: "Internal server error",
                message: error.message
            })
        } else {
            return response.status(204).json() //status 204: Khi xóa thành công
        }
    })
}

//export module
module.exports = { createProduct, getAllProduct, getProductById, updateProduct, deleteProduct };