//Khai báo mongoose
const mongoose = require("mongoose");
//import module models 
const OrderModel = require("../model/OrderModel");
const CustomerModel = require("../model/CustomerModel");

//Hàm tạo Order theo customerid
const createOrderOfCustomer = (request, response) => {
    //lấy id từ request param
    let CustomerId = request.params.CustomerId;
    // Kiểm tra CustomerId có phải ObjectId hay không
    if (!mongoose.Types.ObjectId.isValid(CustomerId)) {
        return response.status(400).json({ //status 400: Khi input truyền vào từ request (query, params, body JSON) không hợp lệ
            status: "Bad request",
            message: "CustomerId is not valid"
        })
    }

    //khai báo biến lấy dữ liệu từ request body json
    let orderDate = request.body.orderDate;
    let shippedDate = request.body.shippedDate;
    let note = request.body.note;
    let cost = request.body.cost;
    let orderDetail = request.body.orderDetail;

    //.create Thêm dữ liệu vào collection
    OrderModel.create({
        _id: mongoose.Types.ObjectId(),
        orderDate: orderDate,
        shippedDate: shippedDate,
        note: note,
        cost: cost,
        orderDetail: orderDetail,
    }, (error, newOrder) => {
        if (error) {
            return response.status(500).json({ //status 500: Khi code bị lỗi
                "status": "Internal server error",
                "messager": error.message,
            })
        }
        else {
            CustomerModel.findByIdAndUpdate(CustomerId, { //Cập nhật dữ liệu customer trên collection theo id
                $push: { //$push: toán tử nối giá trị được chỉ định vào một mảng
                    orders: newOrder._id
                }
            }, (err, data) => {
                if (err) {
                    return response.status(500).json({ //status 500: Khi code bị lỗi
                        "status": "Internal server error",
                        "messager": err.message,
                    })
                }
                else {
                    return response.status(201).json({ //status 201: Khi tạo dữ liệu thành công
                        "status": "Created!",
                        "data": newOrder,
                    })
                }
            })
        }
    })
}

//Hàm lấy toàn bộ danh sách order
const getAllOrder = (request, response) => {
    //lấy dữ liệu từ request param
    let { orderid } = request.query;
    //tạo mảng chứa danh sách điều kiện lọc
    const condition = {};

    if (orderid) {
        condition._id = orderid
    }

    //.find Lấy dữ liệu từ collection
    OrderModel.find(condition).exec((error, data) => {
        if (error) {
            return response.status(500).json({ //status 500: Khi code bị lỗi
                "status": "Internal server error",
                "messager": error.message,
            })
        }
        else {
            return response.status(200).json({ //status 200: Khi response trả về thành công
                "status": "Success",
                "data": data,
            })
        }
    })
}

//Hàm lấy toàn bộ danh sách order của 1 customer
const getAllOrderOfCustomer = (request, response) => {
    //lấy id từ request param
    let CustomerId = request.params.CustomerId;
    let { orderid } = request.query;
    
    // Kiểm tra CustomerId có phải ObjectId hay không
    if (!mongoose.Types.ObjectId.isValid(CustomerId)) {
        return response.status(400).json({ //status 400: Khi input truyền vào từ request (query, params, body JSON) không hợp lệ
            status: "Bad request",
            message: "CustomerId is not valid"
        })
    }

    CustomerModel
        .findById(CustomerId) //Lấy dữ liệu customer từ collection theo id
        //.populate("orders") //populate: tự động thay thế các paths trong documents gốc bằng cách join các document từ các collections khác
        .populate({
            path: 'orders',
            match: { _id: orderid },
            options: { sort: { "timeCreated": "descending" } },
        })
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({ //status 500: Khi code bị lỗi
                    "status": "Internal server error",
                    "messager": error.message,
                })
            }
            else {
                if (data) {
                    return response.status(200).json({ //status 200: Khi response trả về thành công
                        "status": "Success",
                        "data": data.orders
                    })
                }
                else {
                    return response.status(404).json({ //status 404: Khi không tìm thấy bản ghi trên CSDL
                        status: "Not found"
                    })
                }
            }
        })
}

//Hàm lấy thông tin order bởi orderId
const getOrderById = (request, response) => {
    //lấy id từ request param
    let OrderId = request.params.OrderId;
    // Kiểm tra orderId có phải ObjectId hay không
    if (!mongoose.Types.ObjectId.isValid(OrderId)) {
        return response.status(400).json({ //status 400: Khi input truyền vào từ request (query, params, body JSON) không hợp lệ
            status: "Bad request",
            message: "OrderId is not valid"
        })
    }

    //.findById Lấy dữ liệu từ collection theo id
    OrderModel.findById(OrderId, (error, data) => {
        if (error) {
            return response.status(500).json({ //status 500: Khi code bị lỗi
                status: "Internal server error",
                message: error.message
            })
        } else {
            if (data) {
                return response.status(200).json({ //status 200: Khi response trả về thành công
                    status: "Success",
                    data: data
                })
            } else {
                return response.status(404).json({ //status 404: Khi không tìm thấy bản ghi trên CSDL
                    status: "Not found"
                })
            }
        }
    })
}

//hàm cập nhật thông tin order
const updateOrder = (request, response) => {
    //lấy id từ request param
    let OrderId = request.params.OrderId;
    // Kiểm tra orderId có phải ObjectId hay không
    if (!mongoose.Types.ObjectId.isValid(OrderId)) {
        return response.status(400).json({ //status 400: Khi input truyền vào từ request (query, params, body JSON) không hợp lệ
            status: "Bad request",
            message: "OrderId is not valid"
        })
    }

    //khai báo biến lấy dữ liệu từ request body json
    let orderDate = request.body.orderDate;
    let shippedDate = request.body.shippedDate;
    let note = request.body.note;
    let cost = request.body.cost;
    let orderDetail = request.body.orderDetail;

    //.findByIdAndUpdate Cập nhật dữ liệu trên collection theo id
    OrderModel.findByIdAndUpdate(OrderId, {
        orderDate: orderDate,
        shippedDate: shippedDate,
        note: note,
        cost: cost,
        orderDetail: orderDetail,
        timeUpdated: Date.now(),
    }, (error, data) => {
        if (error) {
            return response.status(500).json({ //status 500: Khi code bị lỗi
                status: "Internal server error",
                message: error.message
            })
        } else {
            if (data) {
                return response.status(200).json({ //status 200: Khi response trả về thành công
                    status: "Success",
                    data: data
                })
            } else {
                return response.status(404).json({ //status 404: Khi không tìm thấy bản ghi trên CSDL
                    status: "Not found"
                })
            }
        }
    })
}

//hàm xóa thông tin order của customer
const deleteOrder = (request, response) => {
    //lấy id từ request param
    let OrderId = request.params.OrderId;
    // Kiem tra OrderId co phai ObjectId hay khong
    if (!mongoose.Types.ObjectId.isValid(OrderId)) {
        return response.status(400).json({ //status 400: Khi input truyền vào từ request (query, params, body JSON) không hợp lệ
            status: "Bad request",
            message: "OrderId is not valid"
        })
    }

    //lấy id từ request param
    let CustomerId = request.params.CustomerId;
    // Kiem tra CustomerId co phai ObjectId hay khong
    if (!mongoose.Types.ObjectId.isValid(CustomerId)) {
        return response.status(400).json({ //status 400: Khi input truyền vào từ request (query, params, body JSON) không hợp lệ
            status: "Bad request",
            message: "CustomerId is not valid"
        })
    }

    //.findByIdAndDelete Xóa dữ liệu trên collection theo id
    OrderModel.findByIdAndDelete(OrderId, (error) => {
        if (error) {
            return response.status(500).json({ //status 500: Khi code bị lỗi
                status: "Internal server error",
                message: error.message
            })
        } else {
            CustomerModel.findByIdAndUpdate(CustomerId, { //Cập nhật dữ liệu customer trên collection theo id
                $pull: { //$pull: toán tử loại bỏ khỏi một mảng hiện có tất cả các trường hợp của một giá trị hoặc các giá trị phù hợp với một điều kiện được chỉ định.
                    orders: OrderId
                }
            }, (err) => {
                if (err) {
                    return response.status(500).json({ //status 500: Khi code bị lỗi
                        status: "Internal server error",
                        message: err.message
                    })
                }
                else return response.status(204).json() //status 204: Khi xóa thành công
            })
        }
    })
}

//export module
module.exports = { createOrderOfCustomer, getAllOrderOfCustomer, getOrderById, updateOrder, deleteOrder, getAllOrder }
