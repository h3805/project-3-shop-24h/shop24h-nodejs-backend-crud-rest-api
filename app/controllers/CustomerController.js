//Khai báo mongoose
const mongoose = require("mongoose");
//import module models 
const CustomerModel = require("../model/CustomerModel");

//Hàm tạo Customer
const createCustomer = (request, response) => {
    //khai báo biến lấy dữ liệu từ request body json
    let fullName = request.body.fullName;
    let phone = request.body.phone;
    let email = request.body.email;
    let address = request.body.address;
    let city = request.body.city;
    let country = request.body.country;
    
    //validate
    if (!fullName) {
        return response.status(500).json({ //status 500: Khi code bị lỗi
            "status": "Bad request",
            "messager": "fullName is reqrired",
        })
    }
    if (!phone) {
        return response.status(500).json({ //status 500: Khi code bị lỗi
            "status": "Bad request",
            "messager": "phone is reqrired",
        })
    }
    if (!email) {
        return response.status(500).json({ //status 500: Khi code bị lỗi
            "status": "Bad request",
            "messager": "email is reqrired",
        })
    }
    
    //.create Thêm dữ liệu vào collection
    CustomerModel.create({
        _id: mongoose.Types.ObjectId(),
        fullName: fullName,
        phone: phone,
        email: email,
        address: address,
        city: city,
        country: country,
    }, (error, data) => {
        if (error) {
            return response.status(500).json({ //status 500: Khi code bị lỗi
                "status": "Internal server error",
                "messager": error.message,
            })
        }
        else {
            return response.status(201).json({ //status 201: Khi tạo dữ liệu thành công
                "status": "Created!",
                "data": data,
            })
        }
    })
}

//Hàm lấy toàn bộ danh sách customer
const getAllCustomer = (request, response) => {
    //lấy dữ liệu từ request param
    let { email } = request.query;
    //tạo mảng chứa danh sách điều kiện lọc
    const condition = {};

    if (email) {
        const regex = new RegExp(`${email}`, `i`) //tạo regex /{email}/i
        condition.email = regex // /{email}/i : Tìm các collection có thuộc tính email chứa chữ “{email}”
    }

    //.find Lấy dữ liệu từ collection
    CustomerModel.find(condition).exec((error, data) => {
        if (error) {
            return response.status(500).json({ //status 500: Khi code bị lỗi
                "status": "Internal server error",
                "messager": error.message,
            })
        }
        else {
            return response.status(200).json({ //status 200: Khi response trả về thành công
                "status": "Success",
                "data": data,
            })
        }
    })
}

//Hàm lấy thông tin customer bởi customerId
const getCustomerById = (request, response) => {
    //lấy id từ request param
    let CustomerId = request.params.CustomerId;

    // Kiểm tra CustomerId có phải ObjectId hay không
    if (!mongoose.Types.ObjectId.isValid(CustomerId)) {
        return response.status(400).json({ //status 400: Khi input truyền vào từ request (query, params, body JSON) không hợp lệ
            status: "Bad request",
            message: "CustomerId is not valid"
        })
    }

    //.findById Lấy dữ liệu từ collection theo id
    CustomerModel.findById(CustomerId, (error, data) => {
        if (error) {
            return response.status(500).json({ //status 500: Khi code bị lỗi
                status: "Internal server error",
                message: error.message
            })
        } else {
            if (data) {
                return response.status(200).json({ //status 200: Khi response trả về thành công
                    status: "Success",
                    data: data
                })
            } else {
                return response.status(404).json({ //status 404: Khi không tìm thấy bản ghi trên CSDL
                    status: "Not found"
                })
            }
        }
    })
}

//hàm cập nhật thông tin customer
const updateCustomerById = (request, response) => {
    //lấy id từ request param
    let CustomerId = request.params.CustomerId;
    
    // Kiểm tra CustomerId có phải ObjectId hay không
    if (!mongoose.Types.ObjectId.isValid(CustomerId)) {
        return response.status(400).json({ //status 400: Khi input truyền vào từ request (query, params, body JSON) không hợp lệ
            status: "Bad request",
            message: "CustomerId is not valid"
        })
    }

    //khai báo biến lấy dữ liệu từ request body json
    let { fullName,phone, email, address,city, country  } = request.body;

    //.findByIdAndUpdate Cập nhật dữ liệu trên collection theo id
    CustomerModel.findByIdAndUpdate(CustomerId, {
        fullName: fullName,
        phone: phone,
        email: email,
        address: address,
        city: city,
        country: country,
        timeUpdated: Date.now(),
    }, (error, data) => {
        if (error) {
            return response.status(500).json({ //status 500: Khi code bị lỗi
                status: "Internal server error",
                message: error.message
            })
        } else {
            if (data) {
                return response.status(200).json({ //status 200: Khi response trả về thành công
                    status: "Success",
                    data: data
                })
            } else {
                return response.status(404).json({ //status 404: Khi không tìm thấy bản ghi trên CSDL
                    status: "Not found"
                })
            }
        }
    })
}

//hàm xóa thông tin customer
const deleteCustomerById = (request, response) => {
    //lấy id từ request param
    let CustomerId = request.params.CustomerId;
    
    // Kiểm tra CustomerId có phải ObjectId hay không
    if (!mongoose.Types.ObjectId.isValid(CustomerId)) {
        return response.status(400).json({ //status 400: Khi input truyền vào từ request (query, params, body JSON) không hợp lệ
            status: "Bad request",
            message: "CustomerId is not valid"
        })
    }

    //.findByIdAndDelete Xóa dữ liệu trên collection theo id
    CustomerModel.findByIdAndDelete(CustomerId, (error) => {
        if (error) {
            return response.status(500).json({ //status 500: Khi code bị lỗi
                status: "Internal server error",
                message: error.message
            })
        } else {
            return response.status(204).json() //status 204: Khi xóa thành công
        }
    })
}

//export module
module.exports = { createCustomer, getAllCustomer, getCustomerById, updateCustomerById, deleteCustomerById }
