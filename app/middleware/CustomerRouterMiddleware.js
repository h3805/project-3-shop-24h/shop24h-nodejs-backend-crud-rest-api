const printCustomerUrlMiddleware = (request, response, next) => {
    console.log("Request Customer URL: ", request.url);
    next();
}
module.exports = { printCustomerUrlMiddleware }