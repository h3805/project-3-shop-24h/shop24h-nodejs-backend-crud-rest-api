const printOrderUrlMiddleware = (request, response, next) => {
    console.log("Request Order URL: ", request.url);
    next();
}
module.exports = { printOrderUrlMiddleware }