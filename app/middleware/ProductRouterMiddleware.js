const printProductUrlMiddleware = (request, response, next) => {
    console.log("Request Product URL: ", request.url);
    next();
}
module.exports = { printProductUrlMiddleware }