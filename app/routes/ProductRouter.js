//import express
const express = require("express");
//import Middleware
const { printProductUrlMiddleware } = require("../middleware/ProductRouterMiddleware");
//import controllers
const { createProduct, getAllProduct, getProductById, updateProduct, deleteProduct } = require("../controllers/ProductController");

//khai báo router
const router = express.Router();

router.get("/Products", printProductUrlMiddleware, getAllProduct);
router.post("/Products", printProductUrlMiddleware, createProduct);
router.get("/Products/:ProductId", printProductUrlMiddleware, getProductById);
router.put("/Products/:ProductId", printProductUrlMiddleware, updateProduct);
router.delete("/Products/:ProductId", printProductUrlMiddleware, deleteProduct);

// export router;
module.exports = router;