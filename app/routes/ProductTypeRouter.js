//import express
const express = require("express");
//import Middleware
const { printProductTypeUrlMiddleware } = require("../middleware/ProductTypeRouterMiddleware");
//import controllers
const { createProductType, getAllProductType, getProductTypeById, updateProductType, deleteProductType } = require("../controllers/ProductTypeController");

//khai báo router
const router = express.Router();

router.get("/ProductTypes", printProductTypeUrlMiddleware, getAllProductType);
router.post("/ProductTypes", printProductTypeUrlMiddleware, createProductType);
router.get("/ProductTypes/:ProductTypeId", printProductTypeUrlMiddleware, getProductTypeById);
router.put("/ProductTypes/:ProductTypeId", printProductTypeUrlMiddleware, updateProductType);
router.delete("/ProductTypes/:ProductTypeId", printProductTypeUrlMiddleware, deleteProductType);

// export router;
module.exports = router;