//import express
const express = require("express");
//import Middleware
const { printOrderUrlMiddleware } = require("../middleware/OrderRouterMiddleware");
//import controllers
const { createOrderOfCustomer, getAllOrderOfCustomer, getOrderById, updateOrder, deleteOrder, getAllOrder } = require("../controllers/OrderController");

//khai báo router
const router = express.Router();

router.get("/Customers/:CustomerId/Orders", printOrderUrlMiddleware, getAllOrderOfCustomer);
router.post("/Customers/:CustomerId/Orders", printOrderUrlMiddleware, createOrderOfCustomer);
router.get("/Orders/", printOrderUrlMiddleware, getAllOrder);
router.get("/Orders/:OrderId", printOrderUrlMiddleware, getOrderById);
router.put("/Orders/:OrderId", printOrderUrlMiddleware, updateOrder);
router.delete("/Customers/:CustomerId/Orders/:OrderId", printOrderUrlMiddleware, deleteOrder);


// export router
module.exports = router;