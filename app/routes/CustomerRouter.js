//import express
const express = require("express");
//import Middleware
const { printCustomerUrlMiddleware } = require("../middleware/CustomerRouterMiddleware");
//import controllers
const { createCustomer, getAllCustomer, getCustomerById, updateCustomerById, deleteCustomerById } = require("../controllers/CustomerController");

//khai báo router
const router = express.Router();

router.get("/Customers", printCustomerUrlMiddleware, getAllCustomer);
router.post("/Customers", printCustomerUrlMiddleware, createCustomer);
router.get("/Customers/:CustomerId", printCustomerUrlMiddleware, getCustomerById);
router.put("/Customers/:CustomerId", printCustomerUrlMiddleware, updateCustomerById);
router.delete("/Customers/:CustomerId", printCustomerUrlMiddleware, deleteCustomerById);

// export router;
module.exports = router;